import org.junit.Test;
import org.mockito.Mockito;

import java.io.PrintStream;
import java.util.Scanner;

import static org.junit.Assert.assertEquals;

public class GameTest {

    @Test
    public void shouldStartTheGame() {
        Player player1 = Mockito.mock(Player.class);
        Mockito.when(player1.selectMove()).thenReturn(MoveType.COOPERATE);

        Player player2 = Mockito.mock(Player.class);
        Mockito.when(player2.selectMove()).thenReturn(MoveType.COOPERATE);

        Machine machine = Mockito.mock(Machine.class);
        Score expectedOutput = new Score(machine.BOTH_COOPERATES, machine.BOTH_COOPERATES);
        Mockito.when(machine.score(MoveType.COOPERATE, MoveType.COOPERATE)).thenReturn(expectedOutput);

        PrintStream out = Mockito.mock(PrintStream.class);

        Game trustEvolution = new Game(player1, player2, machine, out);

        trustEvolution.play();

        Mockito.verify(player1).selectMove();
        Mockito.verify(player2).selectMove();
        Mockito.verify(out).println(expectedOutput.toString());
    }

    @Test
    public void shouldPlayManyRounds() {

        Scanner in = new Scanner("COOPERATE\nCOOPERATE\nCOOPERATE\nCOOPERATE\nCOOPERATE\nCOOPERATE\nCOOPERATE\nCOOPERATE\nCOOPERATE\nCOOPERATE\n");
        PlayerBehaviour playerBehaviour = new ConsolePlayerBehaviour(in);
        Player player1 = new Player(playerBehaviour);
        Player player2 = new Player(playerBehaviour);
        Machine machine = new Machine();
        PrintStream out = new PrintStream(System.out);
        Game game = new Game(player1, player2, machine, out);
        game.startTournament(5);
        assertEquals(10, player1.getScore());
        assertEquals(10, player2.getScore());

    }

    @Test
    public void shouldPlayManyRoundsForCoolPlayerWithConsolePlayer() {

        Scanner in = new Scanner("COOPERATE\nCOOPERATE\nCOOPERATE\nCOOPERATE\nCHEAT");
        PlayerBehaviour playerBehaviour = new ConsolePlayerBehaviour(in);
        Player player1 = new Player(playerBehaviour);
        Player player2 = new Player(new CoolPlayerBehaviour());
        Machine machine = new Machine();
        PrintStream out = new PrintStream(System.out);
        Game game = new Game(player1, player2, machine, out);
        game.startTournament(5);
        assertEquals(11, player1.getScore());
        assertEquals(7, player2.getScore());

    }

    @Test
    public void shouldPlayManyRoundsForCheatPlayerWithConsolePlayer() {

        Scanner in = new Scanner("COOPERATE\nCOOPERATE\nCOOPERATE\nCOOPERATE\nCHEAT");
        PlayerBehaviour playerBehaviour = new ConsolePlayerBehaviour(in);
        Player player1 = new Player(playerBehaviour);
        Player player2 = new Player(new CheatPlayerBehaviour());
        Machine machine = new Machine();
        PrintStream out = new PrintStream(System.out);
        Game game = new Game(player1, player2, machine, out);
        game.startTournament(5);
        assertEquals(-4, player1.getScore());
        assertEquals(12, player2.getScore());

    }

    @Test
    public void shouldPlayManyRoundsForCoolPlayerWithCheatPlayer() {

        Player player1 = new Player(new CheatPlayerBehaviour());
        Player player2 = new Player(new CoolPlayerBehaviour());
        Machine machine = new Machine();
        PrintStream out = new PrintStream(System.out);
        Game game = new Game(player1, player2, machine, out);
        game.startTournament(5);
        assertEquals(15, player1.getScore());
        assertEquals(-5, player2.getScore());

    }
}
