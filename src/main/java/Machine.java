public class Machine {

    public static final int OPPONENT_CHEATS = -1;
    public static final int OPPONENT_COOPERATES = 3;
    public static final int BOTH_CHEATS = 0;
    public static final int BOTH_COOPERATES = 2;

    public Score score(MoveType move1, MoveType move2) {
        if (MoveType.COOPERATE == move1 && MoveType.COOPERATE == move2)
            return new Score(BOTH_COOPERATES, BOTH_COOPERATES);
        else if (MoveType.CHEAT == move1 && MoveType.CHEAT == move2)
            return new Score(BOTH_CHEATS, BOTH_CHEATS);
        else if (MoveType.COOPERATE == move1 && MoveType.CHEAT == move2)
            return new Score(OPPONENT_CHEATS, OPPONENT_COOPERATES);
        else
            return new Score(OPPONENT_COOPERATES, OPPONENT_CHEATS);
    }
}
