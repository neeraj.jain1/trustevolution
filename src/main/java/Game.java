import java.io.PrintStream;

public class Game {

    private Player player1;
    private Player player2;
    private Machine machine;
    private PrintStream out;

    public Game(Player player1, Player player2, Machine machine, PrintStream out) {
        this.player1 = player1;
        this.player2 = player2;
        this.machine = machine;
        this.out = out;
    }

    public void play() {
        Score result = machine.score(player1.selectMove(), player2.selectMove());
        player1.updateScore(result.getPlayer1Score());
        player2.updateScore(result.getPlayer2Score());
        out.println(result.toString());
    }

    public void startTournament(int numOfRounds) {
        for(int i=0;i<numOfRounds;i++){
            play();
        }
        out.println(player1.getScore()+","+player2.getScore());
        System.out.println(player1.getScore()+","+player2.getScore());
    }
}
