public class CheatPlayerBehaviour implements PlayerBehaviour {

    @Override
    public MoveType makeMove() {
        return MoveType.CHEAT;
    }
}
