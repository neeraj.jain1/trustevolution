public class CoolPlayerBehaviour implements PlayerBehaviour{

    @Override
    public MoveType makeMove() {
        return MoveType.COOPERATE;
    }
}
